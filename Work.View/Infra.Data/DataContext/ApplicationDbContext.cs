﻿

using Microsoft.AspNet.Identity.EntityFramework;
using Work.Domain.Entities.EntityUser;

namespace Infra.Data.DataContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("MyWorkDb", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
