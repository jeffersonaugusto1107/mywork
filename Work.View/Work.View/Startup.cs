﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Work.View.Startup))]
namespace Work.View
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
